# Schematics for MC 1.20.1

## Underground Water Feature

This is a room I found in a generated overworld dungeon. It features lots of mossy stone and bricks, iron railings, water pouring out of the outer wall, and a decorative ceiling. I *think* everything in this one is Vanilla MC items.